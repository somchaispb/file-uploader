package helpers

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

func GetEnvWithKey(key string) string  {
	return os.Getenv(key)
}

func LoadEnv() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}
}
