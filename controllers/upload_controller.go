package controllers

import (
	"bitbucket.org/somchaispb/file-uploader/helpers"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

func UploadFile(c *gin.Context) {
	sess := c.MustGet("sess").(*session.Session)
	uploader := s3manager.NewUploader(sess)

	MyBucket := helpers.GetEnvWithKey("AWS_BUCKET")
	MyRegion := helpers.GetEnvWithKey("AWS_REGION")
	AwsPath := helpers.GetEnvWithKey("AWS_PATH")

	filePath := c.Request.PostFormValue("path")

	if !strings.HasSuffix(filePath, "/"){
		filePath += "/"
	}

	file, header, err := c.Request.FormFile("file")

	if err != nil {
		panic(err)
	}

	fileName := header.Filename

	// Concurrent goroutine implementation
	upload := func() {
		_, err := uploader.Upload(&s3manager.UploadInput{
			Bucket: aws.String(MyBucket),
			ACL:    aws.String("public-read"),
			Key:    aws.String(AwsPath + filePath + fileName),
			Body:   file,
		})
		if err != nil {
			fmt.Println(err)
			c.JSON(http.StatusInternalServerError, gin.H{
				"error":  "File upload failed",
			})
			return
		}
	}

	go upload()

	// Implementation that waits for full upload
		//up, err := uploader.Upload(&s3manager.UploadInput{
		//	Bucket: aws.String(MyBucket),
		//	ACL:    aws.String("public-read"),
		//	Key:    aws.String(AwsPath + fileName),
		//	Body:   file,
		//})
		//if err != nil {
		//	fmt.Println(err)
		//	c.JSON(http.StatusInternalServerError, gin.H{
		//		"error":  "File upload failed",
		//		"uploader": up,
		//	})
		//	return
		//}

	filepath := "https://" + MyBucket + "." + "s3-" + MyRegion + ".amazonaws.com/" + AwsPath + filePath + fileName

	c.JSON(http.StatusCreated, gin.H{
		"data": filepath,
		"code": http.StatusCreated,
	})

}

func GetFile(c *gin.Context) {

	MyBucket := helpers.GetEnvWithKey("AWS_BUCKET")
	AwsPath := helpers.GetEnvWithKey("AWS_PATH")
	item := c.Query("path")
	tmpFolder := "tmp/"

	pathSlice := strings.Split(item, "/")

	fileName := pathSlice[len(pathSlice) - 1]

	file, err := os.Create(tmpFolder + fileName)

	if err != nil {
		log.Fatalf("Unable to open file %q, %v", fileName, err)
	}

	defer os.Remove(file.Name())

	sess := c.MustGet("sess").(*session.Session)

	downloader := s3manager.NewDownloader(sess)

	_, err2 := downloader.Download(file,
		&s3.GetObjectInput{
			Bucket: aws.String(MyBucket),
			Key:    aws.String(AwsPath + item),
		})

	if err2 != nil {
		log.Fatalf("Unable to download item %q, %v", item, err)
	}

	FileHeader := make([]byte, 512)
	//transferFile, err := ioutil.ReadFile(tmpFolder + fileName)
	FileContentType := http.DetectContentType(FileHeader)
	FileStat, _ := file.Stat()
	FileSize := strconv.FormatInt(FileStat.Size(), 10)

	c.Header("Content-Disposition", "attachment; filename="+fileName)
	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Type", FileContentType)
	c.Header("Content-Length", FileSize)
	c.Header("Content-Type", "application/octet-stream")

	c.File(tmpFolder + fileName)

	//c.Data(http.StatusOK, "application/octet-stream", transferFile)
}

func AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {

		crmURL := os.Getenv("CRM_BASE_URL")

		header := c.GetHeader("Authorization")

		if len(header) == 0 {
			c.Abort()
			c.JSON(http.StatusUnauthorized, gin.H{
				"data": "Token header is missing",
				"code": http.StatusUnauthorized,
			})
			return
		}

		fmt.Println(header)

		client := &http.Client{}
		req, _ := http.NewRequest("GET", crmURL + "api/auth/user", nil)
		req.Header.Set("Authorization", header)
		res, err := client.Do(req)

		if err != nil {
			log.Println(err)
		}

		fmt.Println(res.StatusCode)

		if res.StatusCode != 200 {
			c.Abort()
			c.JSON(http.StatusUnauthorized, gin.H{
				"data": "Token is not valid",
				"code": http.StatusUnauthorized,
			})
			return
		}
	}

}
