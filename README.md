# Experimental File uploader Golang

 ## Local usage
 
 In order to use this app locally, you should perform the following actions:
 * Copy .env.example to .env
 * Define your variables
 * Run: 
 ```go build file-uploader.go```
 * Run ```./file-uploader``` 
* The app will be served on localhost:4000

### Available endpoints:
* /api/v1/upload POST
* /api/v1/download GET

### Uploading
For uploading file you need to send form-data request with a file and filepath which will be used for AWS 
save path. The filename will be parsed from file.
```path = app/test/asd/123```

### Downloading
For downloading file you need to send GET request with additional param ```?path=app/test/asd/123/0223.pdf```
The download will start as soon as file is ready on server.

### Authentication
This app is using /api/auth/user URL from CRM in order to check if token is valid. In the future, it is needed to implement 
check token on the fly within the app.

## Docker
this section is under develop. There is a dockerfile and docker-compose inside this app. 
Testing deploy is needed.

### Notes
The application works in go routine concurrently in background for uploading files. Each upload will run in separate Thread.
Another implementation is available, synchronous upload is also available, in that way Frontend will 
wait until file is being successfully uploaded to AWS S3.