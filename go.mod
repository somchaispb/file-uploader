module bitbucket.org/somchaispb/file-uploader

go 1.15

require (
	github.com/appleboy/gin-jwt/v2 v2.6.4 // indirect
	github.com/aws/aws-sdk-go v1.34.18
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	gorm.io/driver/mysql v1.0.1
	gorm.io/gorm v1.20.0
)
