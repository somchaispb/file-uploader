package main

import (
	"bitbucket.org/somchaispb/file-uploader/controllers"
	"bitbucket.org/somchaispb/file-uploader/helpers"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
)


func main() {
	helpers.LoadEnv()

	sess := helpers.ConnectAWS()

	router := gin.Default()

	router.Use(func(c *gin.Context) {
		c.Set("sess", sess)
		c.Next()
	})

	authorized := router.Group("/api/v1")
	authorized.Use(controllers.AuthRequired())
	{
		authorized.POST("/upload", controllers.UploadFile)
		authorized.GET("/download", controllers.GetFile)
	}

	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound,
			gin.H{"error": gin.H{
				"message":         "not found",
				"code": http.StatusNotFound,
			}})
		c.Error(errors.New("failed to find route"))
	})

	_ = router.Run(":4000")
}




